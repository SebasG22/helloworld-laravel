@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard de ToDo</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(session()->has('todo'))
                        <div class="alert alert-success">Tarea creada exitosamente!!</div>
                    @endif

                    @if($errors->any())
                        <div class="alert alert-danger">
                            <strong>Ocurrió un error escribiendo los datos!!
                            </strong>
                            <ul>
                            @foreach ($errors->all() as $error )
                                    <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('create-todo') }}" method="POST">
                        @csrf
                        <input type="text" name="name" class="form-control" placeholder="add to do">
                        <button class="btn btn-lg" type="submit">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
